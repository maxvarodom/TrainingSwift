import RxSwift


example(of: "Test") {

    var value1 = [String]()

    for _ in 0...10 {
        value1.append("12345")
    }

    Observable.from(value1)
        .scan("1", accumulator: { (a, b) -> String in
            var leftCharacterArray = a.characters.reversed().map { Int(String($0))! }
            var rightCharacterArray = b.characters.reversed().map { Int(String($0))! }
            var result = [Int](repeating: 0, count: leftCharacterArray.count + rightCharacterArray.count)

            for leftIndex in 0..<leftCharacterArray.count {
                for rightIndex in 0..<rightCharacterArray.count {

                    let resultIndex = leftIndex + rightIndex

                    result[resultIndex] = leftCharacterArray[leftIndex] * rightCharacterArray[rightIndex] + (resultIndex >= result.count ? 0 : result[resultIndex])
                    if result[resultIndex] > 9 {
                        result[resultIndex + 1] = (result[resultIndex] / 10) + (resultIndex+1 >= result.count ? 0 : result[resultIndex + 1])
                        result[resultIndex] -= (result[resultIndex] / 10) * 10
                    }
                }
            }
            result = Array(result.reversed())
            return result.map { String($0) }.joined(separator: "")
        })
        .subscribe({ (event) in

            print(event.element)
        })
        .dispose()
}
