//: Please build the scheme 'RxSwiftPlayground' first

import XCPlayground
import RxSwift

example(of: "scan") {
    let bag = DisposeBag()
    
    let source = "value"
    
    Observable.of(1,2,3,4,5)
        .scan(source) { s, eachNumber in
            return "\(s) | \(eachNumber)"
        }
        .subscribe(onNext: { value in
            print(value)
        })
        .disposed(by: bag)
}

















