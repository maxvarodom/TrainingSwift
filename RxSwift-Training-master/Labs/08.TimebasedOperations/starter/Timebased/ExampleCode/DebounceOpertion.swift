//
//  DebounceOpertion.swift
//  Timebased
//
//  Created by Olarn U. on 13/12/2560 BE.
//  Copyright © 2560 Olarn U. All rights reserved.
//

import Foundation
import RxSwift

class DebounceOperation: OperationProtocol {

    func observe(on generator: DataGenerator) -> Observable<String> {
        return Observable.create({ observer -> Disposable in

            // Add code for DebounceOperation here
            
            return Disposables.create()
        })
    }
}
