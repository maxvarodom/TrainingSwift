//
//  DelayOperation.swift
//  Timebased
//
//  Created by Olarn U. on 14/12/2560 BE.
//  Copyright © 2560 Olarn U. All rights reserved.
//

import Foundation
import RxSwift

class DelaySubscriptionOperation: OperationProtocol {
    
    func observe(on generator: DataGenerator) -> Observable<String> {
        return Observable.create({ observer -> Disposable in
            
            // Add code for DelaySubscriptionOperation here
            
            return Disposables.create()
        })
    }
}
