//
//  MainTableViewController.swift
//  Timebased
//
//  Created by Olarn U. on 8/12/2560 BE.
//  Copyright © 2560 Olarn U. All rights reserved.
//

import UIKit
import RxSwift

class MainTableViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    // MARK:- UITableView Datasource
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return OperationType.allTypes.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "operationTypeCell")
        cell?.textLabel?.text = OperationType.allTypes[indexPath.row].description
        return cell ?? UITableViewCell()
    }

    // MARK:- UITableView Delegate

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if OperationType(rawValue: indexPath.row) != nil {
            performSegue(withIdentifier: "segueDetail", sender: indexPath.row)
        }
    }
    
    // MARK:- Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard
            let detailViewController = segue.destination as? DetailViewController,
            let row = sender,
            let index = row as? Int else {
                return
        }
        detailViewController.operationType.value = OperationType(rawValue: index) ?? OperationType.undefined
    }

}
