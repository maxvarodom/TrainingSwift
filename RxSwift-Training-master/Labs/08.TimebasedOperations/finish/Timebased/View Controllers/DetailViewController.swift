//
//  DetailViewController.swift
//  Timebased
//
//  Created by Olarn U. on 8/12/2560 BE.
//  Copyright © 2560 Olarn U. All rights reserved.
//

import UIKit
import RxSwift

class DetailViewController: UIViewController {
    
    @IBOutlet weak var buttonStart: UIBarButtonItem!
    @IBOutlet weak var generatedTextView: UITextView!
    @IBOutlet weak var backpressureTextView: UITextView!
    @IBOutlet weak var titleLabel: UILabel!
    
    private let bag = DisposeBag()
    private let dataGenerator = DataGenerator()
    private lazy var operationObject = operationType.value.getOperatorObject()

    var operationType = Variable<OperationType>(OperationType.undefined)
    var timelineState = Variable<Bool>(false)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setTitle(by: self.operationType)
        handleOnStartStop(from: self.timelineState)
        handleWhenGeneratorEmitData(generator: dataGenerator)
    }

    @IBAction func buttonStartTapped(_ sender: Any) {
        self.timelineState.value = !self.timelineState.value
    }
}

// MARK:- Reactive Implementation

extension DetailViewController {
    
    private func setTitle(by type: Variable<OperationType>) {
        operationType.asObservable()
            .subscribe(onNext: { [weak self] (type) in
                self?.titleLabel.text = type.description
            })
            .disposed(by: bag)
    }
    
    private func handleOnStartStop(from state: Variable<Bool>) {
        state.asObservable()
            .subscribe(onNext: { [weak self] isStarted in
                if isStarted {
                    self?.dataGenerator.start()
                    self?.buttonStart.title = "Stop"
                } else {
                    self?.dataGenerator.stop()
                    self?.buttonStart.title = "Start"
                }
            })
            .disposed(by: bag)
    }
    
    private func handleWhenGeneratorEmitData(generator: DataGenerator) {
        showEmitValueInTextView(generator: generator)
        showBackPressureData(generator: generator)
    }
    
    private func showEmitValueInTextView(generator: DataGenerator) {
        generator.observable
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { data in
                if let text = self.generatedTextView.text {
                    var newString = "\(data)\r\n"
                    newString.append(text)
                    self.generatedTextView.text = newString
                }
            })
            .disposed(by: bag)
    }
    
    private func showBackPressureData(generator: DataGenerator) {
        operationObject.observe(on: generator)
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { data in
                if let text = self.backpressureTextView.text {
                    var newString = "\(data)\r\n"
                    newString.append(text)
                    self.backpressureTextView.text = newString
                }
            })
            .disposed(by: bag)
    }
}
