//
//  DebounceOpertion.swift
//  Timebased
//
//  Created by Olarn U. on 13/12/2560 BE.
//  Copyright © 2560 Olarn U. All rights reserved.
//

import Foundation
import RxSwift

class DebounceOperation: OperationProtocol {

    func observe(on generator: DataGenerator) -> Observable<String> {
        return Observable.create({ observer -> Disposable in
            generator.observable
                .debounce(0.5, scheduler: ConcurrentDispatchQueueScheduler(qos: .background))
                .subscribe(onNext: { data in
                    observer.onNext("Allow \(data)")
                })
                .disposed(by: generator.bag)
            return Disposables.create()
        })
    }
}
