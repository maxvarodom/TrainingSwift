//
//  SampleProtocol.swift
//  Timebased
//
//  Created by Olarn U. on 13/12/2560 BE.
//  Copyright © 2560 Olarn U. All rights reserved.
//

import Foundation
import RxSwift

protocol OperationProtocol {
    func observe(on generator: DataGenerator) -> Observable<String>
}

enum OperationType: Int {
    case throttle
    case debounce
    case sample
    case buffer
    case window
    case delay
    case undefined
    
    var description : String {
        switch self {
        case .throttle: return "Throttle"
        case .debounce: return "Debounce"
        case .sample: return "Sample"
        case .buffer: return "Buffer"
        case .window: return "Window"
        case .delay:  return "Delay"
        case .undefined: return "Undefined"
        }
    }
    
    static let allTypes: [OperationType] = [.throttle, .debounce, .sample, .buffer, .window, .delay]
    
    func getOperatorObject() -> OperationProtocol {
        switch self {
        case .throttle: return ThrottleOperation()
        case .debounce: return DebounceOperation()
        case .sample: return SampleOperation()
        case .buffer: return BufferOperation()
        case .window: return WindowOperation()
        case .delay: return DelayOperation()
        case .undefined: return ThrottleOperation()
        }
    }
}
