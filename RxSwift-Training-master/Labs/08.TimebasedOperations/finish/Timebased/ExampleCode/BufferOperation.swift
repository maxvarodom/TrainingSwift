//
//  BufferOperation.swift
//  Timebased
//
//  Created by Olarn U. on 13/12/2560 BE.
//  Copyright © 2560 Olarn U. All rights reserved.
//

import Foundation
import RxSwift

class BufferOperation: OperationProtocol {
    
    let bufferSize = 3
    let bufferTime = 5.0
    
    func observe(on generator: DataGenerator) -> Observable<String> {
        return Observable.create({ [weak self] observer -> Disposable in
            if let strongSelf = self {
                generator.observable
                    .buffer(
                        timeSpan: strongSelf.bufferTime,
                        count: strongSelf.bufferSize,
                        scheduler: ConcurrentDispatchQueueScheduler(qos: .background))
                    .subscribe(onNext: { data in
                        observer.onNext("\(data)")
                    })
                    .disposed(by: generator.bag)
            }
            return Disposables.create()
        })
    }
}
