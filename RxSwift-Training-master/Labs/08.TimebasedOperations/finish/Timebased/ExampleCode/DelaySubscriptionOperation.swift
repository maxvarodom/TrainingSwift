//
//  DelaySubscriptionOperation.swift
//  Timebased
//
//  Created by Olarn U. on 14/12/2560 BE.
//  Copyright © 2560 Olarn U. All rights reserved.
//

import Foundation
import RxSwift

class DelaySubscriptionOperation: OperationProtocol {
    
    private let delayTimespan = 5.0
    
    func observe(on generator: DataGenerator) -> Observable<String> {
        return Observable.create({ [unowned self] observer -> Disposable in
            generator.observable
                .delaySubscription(self.delayTimespan, scheduler: MainScheduler.instance)
                .subscribe(onNext: { data in
                    observer.onNext("Allow \(data)")
                })
                .disposed(by: generator.bag)
            return Disposables.create()
        })
    }
}
