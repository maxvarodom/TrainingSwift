//
//  WindowOperation.swift
//  Timebased
//
//  Created by Olarn U. on 13/12/2560 BE.
//  Copyright © 2560 Olarn U. All rights reserved.
//

import Foundation
import RxSwift

class WindowOperation: OperationProtocol {
    
    private let bufferSize = 3
    private let timeSpanInSecond = 3.0
    private static var runningNumber = 0
    
    func observe(on generator: DataGenerator) -> Observable<String> {
        return Observable.create { (observer) -> Disposable in
            generator.observable
                .window(
                    timeSpan: self.timeSpanInSecond,
                    count: self.bufferSize,
                    scheduler: ConcurrentDispatchQueueScheduler(qos: .background))
                .subscribe(onNext: { dataObservable in
                    
                    WindowOperation.runningNumber = WindowOperation.runningNumber + 1
                    dataObservable.subscribe(onNext: { value in
                        observer.onNext("#\(WindowOperation.runningNumber) - \(value)")
                    }).disposed(by: generator.bag)
                
                }).disposed(by: generator.bag)
            return Disposables.create()
        }
    }
}
