//
//  DelayOperation.swift
//  Timebased
//
//  Created by Olarn U. on 14/12/2560 BE.
//  Copyright © 2560 Olarn U. All rights reserved.
//

import Foundation
import RxSwift

class DelayOperation: OperationProtocol {
    
    private let delayTimespan = 2.0
    
    func observe(on generator: DataGenerator) -> Observable<String> {
        return Observable.create({ [unowned self] observer -> Disposable in
            generator.observable
                .delay(self.delayTimespan,
                       scheduler: ConcurrentDispatchQueueScheduler(qos: .background))
                .subscribe(onNext: { data in
                    observer.onNext("Allow \(data)")
                })
                .disposed(by: generator.bag)
            return Disposables.create()
        })
    }
}
