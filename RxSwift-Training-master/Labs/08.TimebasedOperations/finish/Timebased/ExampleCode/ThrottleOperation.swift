//
//  Throttle.swift
//  Timebased
//
//  Created by Olarn U. on 13/12/2560 BE.
//  Copyright © 2560 Olarn U. All rights reserved.
//

import Foundation
import RxSwift

class ThrottleOperation: OperationProtocol {
    
    func observe(on generator: DataGenerator) -> Observable<String> {
        return Observable.create({ observer -> Disposable in
            generator.observable
                .throttle(1, scheduler: ConcurrentDispatchQueueScheduler(qos: DispatchQoS.background))
                .subscribe(onNext: { data in
                    observer.onNext("Allow \(data)")
                })
                .disposed(by: generator.bag)
            return Disposables.create()
        })
    }
}
