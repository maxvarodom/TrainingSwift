//
//  SampleOperation.swift
//  Timebased
//
//  Created by Olarn U. on 13/12/2560 BE.
//  Copyright © 2560 Olarn U. All rights reserved.
//

import Foundation
import RxSwift

class SampleOperation: OperationProtocol {
    
    private var counter = 0
    private let samplerIntervalInMilliSecond: UInt32 = 1500000
    
    var sampler: Observable<Int> {
        return Observable.create { [weak self] observer -> Disposable in
            DispatchQueue.global().async {
                if let strongSelf = self {
                    while true {
                        usleep(useconds_t(strongSelf.samplerIntervalInMilliSecond))
                        strongSelf.counter = strongSelf.counter + 1
                        observer.onNext(strongSelf.counter)
                    }
                }
            }
            return Disposables.create()
        }
    }
    
    func observe(on generator: DataGenerator) -> Observable<String> {
        return Observable.create({ [weak self] observer -> Disposable in
            if let strongSelf = self {
                generator.observable
                    .sample(strongSelf.sampler)
                    .subscribe(onNext: { data in
                        observer.onNext("Allow \(data)")
                    })
                    .disposed(by: generator.bag)
            }
            return Disposables.create()
        })
    }
}
