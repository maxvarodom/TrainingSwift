//
//  DataGenerator.swift
//  Timebased
//
//  Created by Olarn U. on 13/12/2560 BE.
//  Copyright © 2560 Olarn U. All rights reserved.
//

import Foundation
import RxSwift

class DataGenerator {
    
    private static var maxIntervalInMillisec: UInt32 = 100
    private let millisecMultiplier = 10

    private var index = 0
    private var isOperating = Variable<Bool>(false)
    private let event = Variable<String>("")
    let bag = DisposeBag()

    init(maxIntervalInMS: UInt32 = maxIntervalInMillisec) {
        DataGenerator.maxIntervalInMillisec = maxIntervalInMS
        self.isOperating
            .asObservable()
            .subscribe({ [weak self] _ in
                DispatchQueue.global(qos: .background).async {
                    self?.generateRandomIntervalEvent()
                }
            })
            .disposed(by: bag)
    }
}

// MARK:- Input

extension DataGenerator {
    
    func start() {
        isOperating.value = true
    }
    
    func stop() {
        isOperating.value = false
    }
}

// MARK:- Output

extension DataGenerator {
    
    var observable: Observable<String> {
        return self.event.asObservable().skip(1).share()
    }
}

// MARK:- Internal logic

extension DataGenerator {
    
    private var randomMillisecond: Int {
        
        // Note: arc4random_uniform will random value from zero (0) to maxIntervalInMillisec - 1.
        //       If the value of "maxIntervalInMillisec" equal to 10, the max random number will be 9.
        //       It may lead to misunderstanding for anyone who want to change the interval that
        //          if he/she want max 10ms why it never reach 10ms.
        //       That's why I add "+1" in this random function.
        
        return Int(arc4random_uniform(DataGenerator.maxIntervalInMillisec + 1)) * millisecMultiplier
    }
    
    private func generateRandomIntervalEvent() {
        repeat {
            let randomMS = self.randomMillisecond
            let interval = randomMS * 1000
            usleep(useconds_t(interval))
            if isOperating.value {
                index = index + 1
                self.event.value = "Emit: \(index) - \(randomMS) ms"
            }
        } while isOperating.value
    }
    
}
