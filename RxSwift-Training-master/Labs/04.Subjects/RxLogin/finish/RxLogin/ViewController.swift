//
//  ViewController.swift
//  RxLogin
//
//  Created by Olarn U. on 6/1/2561 BE.
//  Copyright © 2561 Olarn U. All rights reserved.
//

import UIKit
import RxSwift

class ViewController: UIViewController {

    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    
    private let presenter = ViewPresenter()
    private let bag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.prefersLargeTitles = true
        setupPresenterCallback()
        setupOnTextChange()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

// MARK: - Input

extension ViewController {
    
    private func setupOnTextChange() {
        usernameTextField.addTarget(
            self,
            action: #selector(textFieldDidChange(_:)),
            for: UIControlEvents.editingChanged)
        passwordTextField.addTarget(
            self,
            action: #selector(textFieldDidChange(_:)),
            for: UIControlEvents.editingChanged)
        
        textFieldDidChange(self.usernameTextField)
    }
    
    @objc private func textFieldDidChange(_ textField: UITextField) {
        presenter.credential.value = ViewPresenter.Credential(
            username: usernameTextField.text ?? "",
            password: passwordTextField.text ?? "")
    }
}

// MARK: - Output

extension ViewController {
    
    fileprivate func setupPresenterCallback() {
        presenter.isAllowToLogin.asObserver().subscribe(onNext: { isAllow in
            print(isAllow)
            self.loginButton.isUserInteractionEnabled = isAllow
            self.loginButton.backgroundColor =
                isAllow ? UIColor.blue : UIColor.lightGray
        }).disposed(by: bag)
    }
}

