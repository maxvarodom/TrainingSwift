//
//  ViewPresenter.swift
//  RxLogin
//
//  Created by Olarn U. on 6/1/2561 BE.
//  Copyright © 2561 Olarn U. All rights reserved.
//

import Foundation
import RxSwift

class ViewPresenter {
    
    struct Credential {
        var username = ""
        var password = ""
    }
    
    let credential = Variable<Credential>(Credential(username: "", password: ""))
    let isAllowToLogin = PublishSubject<Bool>()
    let bag = DisposeBag()
    
    init() {
        credential.asObservable().subscribe(onNext: { credential in
            let allow = credential.username.count > 7 &&
                credential.password.count > 4
            self.isAllowToLogin.onNext(allow)
        }).disposed(by: bag)
    }
}
