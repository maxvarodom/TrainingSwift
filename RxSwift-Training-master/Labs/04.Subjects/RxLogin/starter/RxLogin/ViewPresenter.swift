//
//  ViewPresenter.swift
//  RxLogin
//
//  Created by Olarn U. on 6/1/2561 BE.
//  Copyright © 2561 Olarn U. All rights reserved.
//

import Foundation
import RxSwift

class ViewPresenter {
    
    struct Credential {
        var username = ""
        var password = ""
    }

}
