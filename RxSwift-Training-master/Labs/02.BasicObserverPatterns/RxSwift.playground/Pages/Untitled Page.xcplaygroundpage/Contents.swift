//: Please build the scheme 'RxSwiftPlayground' first
import PlaygroundSupport
PlaygroundPage.current.needsIndefiniteExecution = true

import RxSwift

example(of: "Hello, World") {

    let one = 1
    let two = 2
    let three = 3

    let observable: Observable<Int> = Observable<Int>.just(one)
    let observable2 = Observable.of(one,two,three)
    let observable3 = Observable.from([one,two,three])

    observable.subscribe { event in
        print(event)
        if let e = event.element{
            print(e)
        }
    }

    observable2.subscribe(onNext: { (value) in
        print(value)
    }, onError: { (error) in
        print("error")
    }, onCompleted: {
        print("complete")
    }, onDisposed: {
        print("disposed")
    })

    observable3.subscribe(onNext:{ value in
        print("observable 3 onNext -> \(value)")
    })

}


