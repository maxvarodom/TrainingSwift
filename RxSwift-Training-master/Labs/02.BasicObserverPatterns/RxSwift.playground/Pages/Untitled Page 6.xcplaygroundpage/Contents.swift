import RxSwift

example(of: "Factory") {

    func getFizzBuzzObservable() -> Observable<String> {
        return Observable<String>.create({ observable in
            for i in 1...15 {
                if i % 3 == 0 && i % 5 == 0{
                    observable.onNext("fizzbuzz")
                } else {
                    if i % 3 == 0 {
                        observable.onNext("fizz")
                    } else {
                        if i % 5 == 0 {
                            observable.onNext("buzz")
                        } else {
                            observable.onNext("\(i)")
                        }
                    }
                }
            }
            return Disposables.create()
        })
    }

    getFizzBuzzObservable()
        .subscribe(onNext: { fizzBuzzValue in
            print(fizzBuzzValue)
        })
}
