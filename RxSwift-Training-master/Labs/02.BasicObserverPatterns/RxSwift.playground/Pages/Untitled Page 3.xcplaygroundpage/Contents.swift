import RxSwift

protocol Consumer{
    func notify(title:String){

    }
}

class Publisher{
    private var consumers = [Consumer]()
    func register(consumer: Consumer)  {
        self.consumers.append(consumers)
    }

    func release(title:String){
        consumers.forEach { (eachConsumer) in
            eachConsumer.notify(title: title)
        }

    }
}

class customer : Consumer{
    func notify(title:String) {
        print("I'v got \(title)")
    }
}

let publisher = Publisher()

let john = customer()
let mike = customer()

publisher.register(Consumer:john)
publisher.register(Consumer:mike)

publisher.release(title:"Harry Potter volume 1 ")
