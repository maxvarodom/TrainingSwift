import RxSwift

example(of: "Just + Function") {

    var number = 0

    func getNumber() -> Int {
        number = number + 1
        print("Return in function -:>",number)
        return number
    }

    Observable.of(getNumber, getNumber, getNumber)
        .subscribe { event in
            print(event.element?())
    }

}
