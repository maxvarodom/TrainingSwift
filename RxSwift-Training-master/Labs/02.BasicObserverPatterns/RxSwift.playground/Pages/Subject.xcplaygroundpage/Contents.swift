import RxSwift

example(of: "Publish Subject") {

    let subject = PublishSubject<String>()
    let bag = DisposeBag()

    subject.subscribe(onNext: { value in
        print(value)
    },onCompleted: {
        print("onCompleted")
    })
    .disposed(by: bag)

    subject.onNext("1")
    subject.onNext("2")
    subject.onCompleted()
    subject.onNext("3")

}
