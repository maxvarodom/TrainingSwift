import RxSwift

let bag = DisposeBag()

//example(of: "Empty") {
//
//    let observale = Observable<String>.empty()
//    observale.subscribe(onNext: { (s) in
//        print(s)
//    }, onError: { (e) in
//        print(e)
//    }, onCompleted: {
//        print("onCompleted")
//    }, onDisposed: {
//        print("Disposed")
//
//    })
//
//}
//
//example(of: "Never") {
//
//    let observale = Observable<Any>.never()
//    observale.subscribe(onNext: { (s) in
//        print(s)
//    }, onError: { (e) in
//        print(e)
//    }, onCompleted: {
//        print("onCompleted")
//    }, onDisposed: {
//        print("Disposed")
//
//    })
//}
//
//example(of: "Disposeable") {
//
//    let obs = Observable.of("1","2","3","4")
//    let disposeable = obs.subscribe(onNext: { value in
//        print(value)
//    })
//    disposeable.disposed(by: bag)
//}

example(of: "Create") {

    let obs = Observable<String>.create({ (ob) -> Disposable in
//        ob.onNext("first")
//        ob.onNext("second")
        for i in 0..<10 {
            if i % 2 == 0 {
                ob.onNext("\(i)")
            }
        }
        ob.onCompleted()
        return Disposables.create()
    })

    obs.subscribe(onNext: { (V) in
        print(V)
    }, onError: { (E) in
        print(E)
    }, onCompleted: {
        print("onCompleted")
    }, onDisposed: {
        print("onDisposed")
    })

}












