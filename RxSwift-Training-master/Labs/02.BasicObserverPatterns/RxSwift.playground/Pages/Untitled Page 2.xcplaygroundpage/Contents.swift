//: [Previous](@previous)

import Foundation

var str = "Hello, playground"

//: [Next](@next)
var tupleValue = ()
let address = =[
"number": "123",
"street": "Avenue Street",
"zipCode":"12345"
]
let number = address["number"]
let s: String = number ?? ""

class customer { // pass by reference

}
struct Student { // pass by value
    <#fields#>
}
