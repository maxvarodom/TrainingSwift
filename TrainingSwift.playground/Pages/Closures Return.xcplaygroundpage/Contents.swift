import Foundation

class Pass {
	func valuePassData(pass: String) -> ((_ pass: String) -> Void) {
		return pass(pass)
	}
}

let initObject = Pass.init()
initObject.valuePassData(pass: <#T##String#>)
