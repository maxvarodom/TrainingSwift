import UIKit
import Foundation

example(of: "Points") {
	
	func exectue()  {
		var nPointsInside = 0
		let view = UIView(frame: CGRect(x: 0, y: 0, width: 200, height: 200))
		view.backgroundColor = UIColor.gray

		for nPoints in [100, 1000, 5000] {
			
			for _ in 1...nPoints {
				let (x, y) = (drand48() * 2 - 1, drand48() * 2 - 1)
				let centerX = (x + 1) * 50
				let centerY = (y + 1) * 50
				let point = UIView(frame: CGRect(x: centerX,y: centerY,width: 1, height: 1))
				
				if x * x + y * y <= 1 {
					nPointsInside += 1
					point.backgroundColor = UIColor.red
				} else {
					point.backgroundColor = UIColor.blue
				}
				view.addSubview(point)
			}
		}
	}
	
	exectue()
}

