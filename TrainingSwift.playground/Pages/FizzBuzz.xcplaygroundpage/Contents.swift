import Foundation

struct FizzBuzz {
	
	func getFizzBuzz(_ number: Int) -> String {
		var result: String = String.init()
		if isDivisible(by: 3, number) { result += "Fizz" }
		if isDivisible(by: 5, number) { result += "Buzz" }
		return result.isEmpty ? String.init(number) : result
	}
	
	func isDivisible(by divisor: Int, _ number: Int) -> Bool {
		return number % divisor == 0
	}
}

var fizzBuzz = FizzBuzz.init()

example(of: "FizzBuzz") {
	for i in 1...25 {
		let output = fizzBuzz.getFizzBuzz(i)
		print(output)
	}
}
