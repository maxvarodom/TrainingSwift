import Foundation

class Col {

    func ad(re: @escaping (String) -> Void) -> Self {
        re("Max")
        return self
    }

    func ac(re: @escaping (String) -> Void) -> Self {
        re("Na")
        return self
    }

    func asd() {
        ad { (Max) in print(Max) }
            .ac { (Na) in print(Na) }
            .ac { (Na) in print(Na) }
            .ac { (Na) in print(Na) }
            .ac { (Na) in print(Na) }
            .ac { (Na) in print(Na) }
    }
}

let col = Col()

col.asd()
