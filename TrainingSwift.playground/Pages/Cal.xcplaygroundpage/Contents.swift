import UIKit

let input1: String = "1000"
let input2: String = "500"
var outPut: String = String()

let byInt1 = input1
	.map { (character) -> Int in
		let toInt = Int(character.description) ?? 0
		return toInt
}

let byInt2 = input2
	.map { (character) -> Int in
		let toInt = Int(character.description) ?? 0
		return toInt
}

let validateOver = byInt1.count > byInt2.count

var shipFirst: Int = Int()
var shipLast: Int = Int()

func ship(by out: Int) -> Int {
	var o: Int = Int()
	
	if out > 9 {
		let i = out.description
		shipFirst = Int(i.first!.description)!
		shipLast = Int(i.last!.description)!
		o = shipLast
	} else {
		shipFirst = 0
		shipLast = 0
		o = out
	}
	return o
}

var bringIn = validateOver ? byInt1 : byInt2

for int in 0...(bringIn.count)  {
	
	var out: Int = Int()
	
	if byInt1.count < int {
		out = 0 + byInt2[int]
	}
	
	if byInt2.count < int {
		out = byInt1[int] + 0
	}
	
	if shipFirst > 0 {
		out = Int(outPut.first!.description)! + shipFirst
		outPut.removeFirst()
		outPut.insert(Character.init(out.description), at: String.Index.init(encodedOffset: 0))
		break
	} else {
		out = byInt1[int] + byInt2[int]
	}
	
	outPut = outPut + "\(ship(by: out))"
}

print(outPut)
