import Foundation

typealias Parameter<Value> = Value

let parameter1: Parameter<String> = "Max"
let parameter2: Parameter<Int> = 12
let parameter3: Parameter<Double> = 12.12
let parameter4: Parameter<Float> = 12.123

typealias ParameterDictionary<Key: Hashable,Value> = Dictionary<Key,Value>

let parameterDictionary1: ParameterDictionary<Int,String> = [1:"A"
	,2:"B"
	,3:"C"
	,4:"D"
	,5:"E"
	,6:"F"
	,7:"G"
	,8:"H"]

parameterDictionary1.map({ key,value in
	print(" Key -> \(key) Value -> \(value)")
})
