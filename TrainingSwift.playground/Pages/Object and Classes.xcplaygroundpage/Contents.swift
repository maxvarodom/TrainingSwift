import Foundation

class Shape {
	var numberOfSides = 0
	
	func simpleDescription() -> String {
		return "A shape with \(numberOfSides) sides."
	}
}

example(of: "Shape") {
	let shape = Shape.init()
	shape.numberOfSides = 7
	let simpleDescription =  shape.simpleDescription()
	print(simpleDescription)
}

class NamedShape {
	var numberOfSides: Int = 0
	var name: String
	
	init(name: String) {
		self.name = name
	}
	
	func simpleDescription() -> String {
		return "A shape with \(numberOfSides) sides."
	}
}

class Square: NamedShape {
	var sideLength: Double
	
	init(sideLength: Double, name: String) {
		self.sideLength = sideLength
		super.init(name: name)
		numberOfSides = 4
	}
	
	func area() -> Double {
		return  sideLength * sideLength
	}
	
	override func simpleDescription() -> String {
		return "A square with sides of length \(sideLength)"
	}
}

example(of: "Square") {
	let square = Square.init(sideLength: 3.3, name: "Square")
	print("area :->",square.area())
}

class EquilateralTruangle: NamedShape {
	var sideLength: Double = 0.0
	
	init(sideLength: Double, name: String) {
		super.init(name: name)
		self.sideLength = sideLength
		numberOfSides = 3
	}
	
	var perimeter: Double {
		get {
			return 3.0 * sideLength
		}
		set {
			sideLength = newValue / 3.0
		}
	}
	
	func area() -> Double {
		return sqrt(3) / 4 * sideLength * sideLength
	}
	
	override func simpleDescription() -> String {
		return "A equilaeral triangle with side of length \(sideLength)"
	}
}

example(of: "EquilateralTruangle") {
	let triangle = EquilateralTruangle.init(sideLength: 3.1, name: "a triangle")
	print(triangle.perimeter)
	triangle.perimeter = 9.9
	print(triangle.sideLength)
	print(triangle.area())
}

class TrangleAndSquare {
	var triangle: EquilateralTruangle {
		willSet {
			square.sideLength = newValue.sideLength
		}
	}
	
	var square: Square {
		willSet {
			triangle.sideLength = newValue.sideLength
		}
	}
	
	init(size: Double, name: String) {
		square = Square.init(sideLength: size, name: name)
		triangle = EquilateralTruangle.init(sideLength: size, name: name)
	}
}

example(of: "triandleAndSquare") {
	let triandleAndSquare = TrangleAndSquare.init(size: 10, name: "another shape")
	print(triandleAndSquare.square.sideLength)
	print(triandleAndSquare.triangle.sideLength)
	
	triandleAndSquare.square = Square.init(sideLength: 50, name: "larger sqaure")
	print(triandleAndSquare.square.sideLength)
}

example(of: "Optional valuus") {
	// Optional valuus
	let optionalSquare: Square? = Square.init(sideLength: 2.5, name: "optional square")
	let sideLength = optionalSquare?.sideLength
	print(sideLength)
}

class Circle: NamedShape {
	
	var radius: Double = 0.0
	
	init(radius: Double ,name: String) {
		super.init(name: name)
		self.radius = radius
	}
	
	var area: Double {
		return Double.pi * radius * radius
	}
	
	var dimeter: Double {
		return radius * 2
	}
	
	var circumference: Double {
		return 2.0 * Double.pi * radius
	}
}

let circlr = Circle.init(radius: 12, name: "PI")
circlr.area
circlr.dimeter
circlr.circumference


