import Foundation

// ใช้ในกรณีที่มี parameter จำนวนเยอะ - > พี่เจ :: ระวัง!

// MARK: - ModelBuilder

class TreeModelModelBuilder {

    var model: EuropeanModel?

    init() {
        model = EuropeanModel()
    }

    func setValue_Pi1_Pi4(pi1: String, pi2: String, pi3: String, pi4: String) -> Self {
        self.model?.pi1 = pi1
        self.model?.pi2 = pi2
        self.model?.pi3 = pi3
        self.model?.pi4 = pi4
        return self
    }

    func setValue_Pi5_Pi7(pi5: String, pi6: String, pi7: String) -> Self {
        self.model?.pi5 = pi5
        self.model?.pi6 = pi6
        self.model?.pi7 = pi7
        return self
    }

    func getModel() -> EuropeanModel {
        return self.model!
    }

}

// MARK: - Model

class EuropeanModel {

    var pi1: String?
    var pi2: String?
    var pi3: String?
    var pi4: String?
    var pi5: String?
    var pi6: String?
    var pi7: String?

    init() { }

    init(pi1: String, pi2: String, pi3: String, pi4: String, pi5: String, pi6: String, pi7: String) {
        self.pi1 = pi1
        self.pi2 = pi2
        self.pi3 = pi3
        self.pi4 = pi4
        self.pi5 = pi5
        self.pi6 = pi6
        self.pi7 = pi7
    }

}

// MARK: - Run

let treeModelModelBuilder = TreeModelModelBuilder()
treeModelModelBuilder
    .setValue_Pi1_Pi4(pi1: "Pi1", pi2: "Pi2", pi3: "Pi3", pi4: "Pi4")
    .setValue_Pi5_Pi7(pi5: "Pi5", pi6: "Pi6", pi7: "Pi7")
    .getModel()

print(treeModelModelBuilder.model?.pi1 as Any)
print(treeModelModelBuilder.model?.pi2 as Any)
print(treeModelModelBuilder.model?.pi3 as Any)
print(treeModelModelBuilder.model?.pi4 as Any)
print(treeModelModelBuilder.model?.pi5 as Any)
print(treeModelModelBuilder.model?.pi6 as Any)
print(treeModelModelBuilder.model?.pi7 as Any)

