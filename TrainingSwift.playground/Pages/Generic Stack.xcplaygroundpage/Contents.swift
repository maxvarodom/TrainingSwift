import Foundation

struct Stack<Element> {

    var items = [Element]()

    mutating func push(_ item: Element) {
        items.append(item)
    }

    mutating func pop() -> Element {
        return items.removeLast()
    }
}

// ===== Stack =====
example(of: "===== Stack Value ====") {
	var stack = Stack<String>()
	stack.push("A")
	stack.push("B")
	stack.push("C")
	stack.push("D")
	stack.push("E")
	print("Value -> , \(stack.items)")
}

// Remove list Element
example(of: "===== Remove list Element ====") {
	var stack = Stack<String>()
	stack.push("A")
	stack.push("B")
	stack.push("C")
	stack.push("D")
	stack.push("E")
	stack.pop()
	print("Value -> , \(stack.items)")
	stack.pop()
	print("Value -> , \(stack.items)")
	stack.pop()
	print("Value -> , \(stack.items)")
	stack.pop()
	print("Value -> , \(stack.items)")
	stack.pop()
	print("Value -> , \(stack.items)")
}
