var numbers = [20, 19, 7, 12]

let out1 = numbers.map { (number: Int) -> Int in
	return 3 * number
}
print(out1)

let out2 = numbers.map { (number) -> Int in
	number * 3
}
print(out2)

let out3 = numbers.map { 3 * $0 }
print(out3)
