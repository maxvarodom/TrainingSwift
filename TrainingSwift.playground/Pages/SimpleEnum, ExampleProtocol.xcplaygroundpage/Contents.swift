protocol ExampleProtocol {
	var simpleDescription: String { get }
	mutating func adjust()
}

class SimpleClass : ExampleProtocol {
	var simpleDescription: String = "Hahahahahaha"
	var anotherProperty : Int = 9237401
	func adjust() {
		simpleDescription += " very SIMPLE CLASS."
	}
}

var simpleVar = SimpleClass()
print(simpleVar.simpleDescription)
simpleVar.adjust()
simpleVar.adjust()
simpleVar.adjust()
simpleVar.adjust()
print(simpleVar.simpleDescription)

struct SimpleStruct : ExampleProtocol {
	var simpleDescription: String = "simple struct"
	mutating func adjust() {
		simpleDescription += ", Really ?"
	}
}


var simpleStruct = SimpleStruct()
print(simpleStruct.simpleDescription)
simpleStruct.adjust()
print(simpleStruct.simpleDescription)


enum SimpleEnum : ExampleProtocol {
	case haha,hoho
	var simpleDescription: String {
		switch self {
		case .haha:
			return "haha"
		case .hoho:
			return "hoho"
		}
	}
	
	mutating func adjust() {
		
	}
}

enum SimpleEnum2 : ExampleProtocol {
	case haveValue(String)
	
	var simpleDescription: String {
		switch self {
		case .haveValue(let description):
			return description
		}
	}
	
	mutating func adjust() {
		switch self {
		case .haveValue( _):
			self = .haveValue(simpleDescription + " (adjust!)")
		}
	}
}

var simpleEnum2 = SimpleEnum2.haveValue("Description")
simpleEnum2.adjust()
print(simpleEnum2)

extension Double: ExampleProtocol {
	var simpleDescription: String {
		return "The number a \(self)"
	}
	
	mutating func adjust() {
		self += 1204.0
	}
}

var example: [ExampleProtocol] = [9.0, SimpleEnum.haha, SimpleClass.init()]

print("------")

for o in example {
	print(o.simpleDescription)
}
