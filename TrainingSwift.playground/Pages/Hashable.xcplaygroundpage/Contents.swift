import Foundation

struct GridPoint {
	var x: Int
	var y: Int
}

extension GridPoint: Hashable {
	var hashValue: Int {
		return x.hashValue ^ y.hashValue &* 16777619
	}
	
	static func == (lhs: GridPoint, rhs: GridPoint) -> Bool {
		return lhs.x == rhs.x && lhs.y == rhs.y
	}
}

var tappedPoints: Set = [GridPoint(x: 2, y: 3), GridPoint(x: 4, y: 1)]

let nextTap = GridPoint(x: 0, y: 1)

if tappedPoints.contains(nextTap) {
	print("Already tapped at (\(nextTap.x), \(nextTap.y)).")
} else {
	tappedPoints.insert(nextTap)
	print("New tap detected at (\(nextTap.x), \(nextTap.y)).")
}
