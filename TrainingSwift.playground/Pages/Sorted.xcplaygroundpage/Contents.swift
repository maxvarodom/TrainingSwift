var numbers = [20, 19, 7, 12]
let sort1 = numbers.sorted { (fist, second) -> Bool in
	return fist < second
}
print(sort1)

let sort2 = numbers.sorted(by: <)
print(sort2)

let sort3 = numbers.sorted()
print(sort3)

let sort4 = numbers.sorted(by: { $0 < $1 })
print(sort4)

func isOddValue(number: Int) -> Bool {
	return number % 2 != 0
}

let match = numbers.map { (number) -> Int in
	return isOddValue(number: number) ? 0 : number
}

print(match)
