import Foundation

let list = [1,2,3,4,5,6,7,8]

example(of: "Enumerated") {
	let a = list
		.enumerated()
		.map { (arg) -> Int in
			return arg.element + 10
	}
	print(a)
}

example(of: "For loop") {
	var coList = [Int]()
	
	for int in list {
		var coInt = int
		coInt = coInt + 10
		coList.append(coInt)
	}
	print(coList)
}


