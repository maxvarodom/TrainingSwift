////10:34 challeee 🌟 . digits(:Int) -> [Int]
////10:34 challeee 🌟 . digits(1234) -> [1,2,3,4]
////10:35 challeee 🌟 . digits(40) -> [4,0]

example(of: "DigitsLogic1") {

    func digitsLogic1(int: Int) -> [Int] {
        let number = String(int)
        return number
            .map { Int($0.description)! }
    }

    print(digitsLogic1(int: 200))
}

example(of: "DigitsLogic2") {

    func digitsLogic2(int: Int) -> [Int] {
        if int < 10 {
            return [int]
        }else {
            let digit = int % 10
            return digitsLogic2(int: int / 10 ) + [digit]
        }
    }

    print(digitsLogic2(int: 200))
}

example(of: "DigitsLogic4") {

    print("200".map { Int($0.description)! })

}
