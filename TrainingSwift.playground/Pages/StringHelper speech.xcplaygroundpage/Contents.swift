import Foundation

class StringHelper {
	
	static let shared = StringHelper()
	let numberFormatter = NumberFormatter()
	
	init() {
		numberFormatter.minimumFractionDigits = 2
		numberFormatter.maximumFractionDigits = 2
		numberFormatter.numberStyle = .decimal
	}
}

extension String {
		
	func numberFormat(_ fraction:Bool = false, defaultText:String = "0") -> String {
		guard let amount = Double(self) else { return self }
		var text = StringHelper.shared.numberFormatter.string(from: NSNumber(value: amount)) ?? defaultText
		if fraction == false {
			text = text.replacingOccurrences(of: ".00", with: "")
		}
		if text.isEmpty {
			text = defaultText
		}
		return text
	}
	
	func speech() -> String {
		var text1 = ""
		var text2 = ""
		let arrayOfString = self.components(separatedBy: ".")
		text1 = (arrayOfString.first ?? "") + " บาท"
		if arrayOfString.count == 2 {
			text2 = (arrayOfString.last ?? "") + " สตางค์"
		}
		return "\(text1)  \(text2)"
	}
}

let speechText = "112.12".numberFormat().speech()
print(speechText)
