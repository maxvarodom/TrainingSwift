import UIKit
import XCPlayground

let path = UIBezierPath()
path.move(to: CGPoint(x:10,y:10))
path.addLine(to: CGPoint(x:290,y:10))
path.lineWidth = 2

let dashPattern : [CGFloat] = [8, 8]
path.setLineDash(dashPattern, count: 2, phase: 0)
path.lineCapStyle = CGLineCap.round
path.lineCapStyle = .butt

let renderer = UIGraphicsImageRenderer(size: CGSize(width: 300, height: 20))
let image = renderer.image { context in
	path.stroke()
}
