import Foundation

class Genaric {

    class func swapTwoValues<T>(_ a: inout T, _ b: inout T) {
        let temporaryA = a
        a = b
        b = temporaryA
    }

}

// ==== Swap Value

print("===== Swap Value ====")

var someString = "hello"
var anotherString = "world"
print("someString -> \(someString) , anotherString -> \(anotherString) ")

_ = Genaric.swapTwoValues(&someString, &anotherString)

print("someString -> \(someString) , anotherString -> \(anotherString) ")

var someInt = 1
var anotherInt = 2
print("someInt -> \(someInt) , anotherInt -> \(anotherInt) ")

_ = Genaric.swapTwoValues(&someInt, &anotherInt)

print("someInt -> \(someInt) , anotherInt -> \(anotherInt) ")

var someFloat = Float(10.10)
var anotherFloat = Float(20.20)
print("someFloat -> \(someFloat) , anotherFloat -> \(anotherFloat) ")

_ = Genaric.swapTwoValues(&someInt, &anotherInt)

print("someFloat -> \(someFloat) , anotherFloat -> \(anotherFloat) ")



