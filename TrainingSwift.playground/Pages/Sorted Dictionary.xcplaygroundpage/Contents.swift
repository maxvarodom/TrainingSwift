import Foundation

let dictionary1 = [
    1:"A",
    2:"B",
    3:"C",
    4:"D",
    5:"E",
    6:"F",
    7:"G",
    8:"H"
]

print("<<<<<<- Ex1 ->>>>>>")

dictionary1
    .sorted(by: { key1,key2 -> Bool in
        return key1 < key2
    })
    .map { key,value in
        print(" Key -> \(key) Value -> \(value)")
}

print("<<<<<<- Ex2 ->>>>>>")

dictionary1
    .sorted(by: < )
    .map { key,value in
        print(" Key -> \(key) Value -> \(value)")
}
