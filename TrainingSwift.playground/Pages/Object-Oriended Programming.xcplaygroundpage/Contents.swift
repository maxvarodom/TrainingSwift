// MARK: - Create Object-Oriended Programming

class Car {

    var id: String?
    var name: String?
    var color: String?
    var size: String?
    var speed: String?

    init(id: String, name: String, color: String, size: String, speed: String) {
        self.id = id
        self.name = name
        self.color = color
        self.size = size
        self.speed = speed
    }
}

// MARK: - Run

example(of: "Car1") {

    let car1 = Car(id: "1", name: "Keeway", color: "black", size: "1400*4000", speed: "120 km/hrs")
    print("id -> \(car1.id!)")
    print("name -> \(car1.name!)")
    print("color -> \(car1.color!)")
    print("size -> \(car1.size!)")
    print("speed -> \(car1.speed!)")

}

example(of: "Car2") {

    let car2 = Car(id: "2", name: "Honda", color: "green", size: "2000*4000", speed: "135 km/hrs")
    print("id -> \(car2.id!)")
    print("name -> \(car2.name!)")
    print("color -> \(car2.color!)")
    print("size -> \(car2.size!)")
    print("speed -> \(car2.speed!)")

}

example(of: "Car3") {

    let car3 = Car(id: "3", name: "Kawasaki", color: "red", size: "1280*3500", speed: "140 km/hrs")
    print("id -> \(car3.id!)")
    print("name -> \(car3.name!)")
    print("color -> \(car3.color!)")
    print("size -> \(car3.size!)")
    print("speed -> \(car3.speed!)")

}

example(of: "Car4") {
    let car4 = Car(id: "4", name: "BMW", color: "blue", size: "3200*1280", speed: "180 km/hrs")
    print("id -> \(car4.id!)")
    print("name -> \(car4.name!)")
    print("color -> \(car4.color!)")
    print("size -> \(car4.size!)")
    print("speed -> \(car4.speed!)")
}
