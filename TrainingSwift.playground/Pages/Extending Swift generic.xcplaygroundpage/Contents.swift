import Foundation

protocol StringProtocol {}
extension String : StringProtocol {}

// Error enum for handling missing mandatory keys

enum ErrorType : Error {
    case NoValueForKey(StringProtocol)
}

public protocol DecodingSupportedType {}

extension String : DecodingSupportedType {}

// MARK: - Logic

extension Dictionary where Key: StringProtocol {

    func decodingKey<ReturnType : DecodingSupportedType>(key: Key) throws -> ReturnType {

        guard let value = self[key] as? ReturnType else {
            throw ErrorType.NoValueForKey(key)
        }
        return value
    }

    func decodingKey<ReturnType : DecodingSupportedType>(key: Key) -> ReturnType? {
        return self[key] as? ReturnType
    }
}

// MARK: - Run

let dictionary: [String: Any] = ["key1": "value1",
                                 "key2": "value2",
                                 "key3": "value3",
                                 "key4": "value4"]

// Handling failures with mandatory keys

do {
    let mandatoryValue1: String = try dictionary.decodingKey(key: "key1")
    let mandatoryValue2: String = try dictionary.decodingKey(key: "key2")
    let mandatoryValue3: String = try dictionary.decodingKey(key: "key3")
    let mandatoryValue4: String = try dictionary.decodingKey(key: "key4")
    print(mandatoryValue1)
    print(mandatoryValue2)
    print(mandatoryValue3)
    print(mandatoryValue4)
} catch let error {
    print(error) // Error -> NoValueForKey("otherkey")
}
