import Foundation

extension Double {
	static let zero: Double = 0.0
}


example(of: "Double") {
	print(Double.zero)
}
