import Foundation

// MARK: Base type genaric -> BaseModel 'pi', 'mi'

class BaseModel {

    var pi: String?
    var mi: String?

    init() {
        self.pi = "PI"
        self.mi = "PI"
    }

}

// MARK: Genaric for type 'Model' -> BaseModel

class Genaric<Model> where Model: BaseModel {

    var model: Model?

    init( _ model: Model) {
        self.model = model
    }

}

// MARK: - Model impremant -> BaseModel Green, Rad

class TreeModelGreen: BaseModel {

    var treeGreenPi: String?
    var treeGreenMi: String?

    init(treeGreenPi: String, treeGreenMi: String) {
        super.init()
        self.treeGreenPi = treeGreenPi
        self.treeGreenMi = treeGreenMi
    }

}

class TreeModelRad: BaseModel {

    var treeRedPi: String?
    var treeBlueMi: String?

    init(treeRedPi: String, treeBlueMi: String) {
        super.init()
        self.treeRedPi = treeRedPi
        self.treeBlueMi = treeBlueMi
    }

}

// MARK: - Use Genaric at class TreeModelGrenn, TreeModelRad for tpye BaseModel 

class UseGenaric1: Genaric<TreeModelGreen> {

    init() {
        super
            .init(TreeModelGreen
                .init(treeGreenPi: "TreePi", treeGreenMi: "treeMi"))
    }
}

class UserGenaric2: Genaric<TreeModelRad> {

    init() {
        super
            .init(TreeModelRad
                .init(treeRedPi: "treeRedPi", treeBlueMi: "treeBlueMi"))
    }

}

// MARK: - Run

print("UseGenaric1")

let useGenaric1 = UseGenaric1.init()

print(useGenaric1.model?.treeGreenMi as Any)
print(useGenaric1.model?.treeGreenPi as Any)
print(useGenaric1.model?.mi as Any)
print(useGenaric1.model?.pi as Any)

print("")
print("==============")
print("")

print("UseGenaric2")

let useGenaric2 = UserGenaric2.init()

print(useGenaric2.model?.treeBlueMi as Any)
print(useGenaric2.model?.treeRedPi as Any)
print(useGenaric2.model?.mi as Any)
print(useGenaric2.model?.pi as Any)

