import Foundation

class Genaric {

    class func passAnyType<S>( _ type: S) -> S {
        return type
    }
}

// ==== Pass Value
example(of: "==== Pass Value ====") {
	let genaric1 = Genaric.passAnyType("Max")
	let genaric2 = Genaric.passAnyType(12)
	let genaric3 = Genaric.passAnyType(31.02)
	
	print("genaric1 -> \(genaric1)")
	print("genaric1 -> \(genaric2)")
	print("genaric1 -> \(genaric3)")
}
