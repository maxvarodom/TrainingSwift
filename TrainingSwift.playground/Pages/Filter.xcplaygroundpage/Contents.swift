import Foundation

// MARK: - Filter String

let string = "key-asdf-faf"

let filter1 = string.filter { cha in
    if cha == "k" {
        return true
    } else {
        return false
    }
}

print("Filter String -> \(filter1)") // -> k

// MARK: - Filter Array

let array = ["value1","value2","value3"]

let filter2 = array.filter { string in
    if string == "value1" {
        return true
    } else {
        return false
    }
}

print("Filter Array -> \(filter2)") // -> ["value1"]

