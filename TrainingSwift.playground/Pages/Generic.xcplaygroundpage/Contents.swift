import Foundation

// Generic

func makeArray<Item>(repeating item: Item, numberOfTimes: Int) -> [Item] {
	var resulf = [Item]()
	for _ in 0...numberOfTimes {
		resulf.append(item)
	}
	return resulf
}

makeArray(repeating: "String", numberOfTimes: 10)

func anyCommon<T: Sequence, U: Sequence>(_ lhs: T, _ rhs: U) -> Bool where T.Iterator.Element: Equatable, T.Iterator.Element == U.Iterator.Element {
	for lhsItem in lhs {
		for rhsItem in rhs {
			if lhsItem == rhsItem {
				return true
			}
		}
	}
	return false
}

anyCommon([1,2,3,4], [5])

func anyCommom2<T: Sequence>( _ lhs: T, _ rhs: T) -> Bool where T.Iterator.Element: Equatable {
	for lhsItem in lhs {
		for rhsItem in rhs {
			if lhsItem == rhsItem {
				return true
			}
		}
	}
	return false
}

anyCommom2([1,2,3,4], [5])
