example(of: "repeat-while statement") {
    var f = 0
    print("Start loop")
    repeat {
        print("Test \(f)")
        f += 1
    } while f <= 5
    print("End loop")
}

example(of: "while statement") {
    var int = 0
    while int <= 5 {
        print("index: \(int)")
        int += 1
    }
}

example(of: "Stop loop") {
    for int in 0...5 {
        if int <= 2 {
            print("Loop continue: ", int)
            continue
        } else {
            print("Stop loop")
            break
        }
    }
}
