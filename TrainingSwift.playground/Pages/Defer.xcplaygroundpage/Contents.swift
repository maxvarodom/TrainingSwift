import Foundation

//: defer
var fridgeIsOpen = false
let fridegeContent = ["milk", "eggs", "leftovers"]

func fridgeContains(_ food: String) -> Bool {
	defer {
		fridgeIsOpen = false
	}
	
	fridgeIsOpen = true
	let resulf = fridegeContent.contains(food)
	return resulf
}

example(of: "Defer false") {
	fridgeContains("banana")
	print(fridgeIsOpen)
}

