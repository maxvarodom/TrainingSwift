import UIKit

// MARK: - M -> Model

struct Model {
    var name: String
    var age: String
}

// MARK: - V -> ViewController

class ViewController: UIViewController, CallPresenter {

    var presenter: Presenter?

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = Presenter(self)
        presenter?.requestSerivce()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }

    func display(response: [Model]) {
        response.forEach { response in
            print(" Name count \(response.name) , Age: \(response.age)")
        }
    }
}

// MARK: - P -> Presenter

class Presenter {

    weak var call: CallPresenter?
    
    init( _ call: CallPresenter) {
        self.call = call
    }

    func requestSerivce() {
        API().serviceGetList { response in
            self.call?.display(response: response)
        }
    }
}

protocol CallPresenter: class {
    func display( response: [Model])
}

class API {
	
    func serviceGetList( _ callback: @escaping ([Model]) -> Void ) {
        var list = [Model]()
        for int in 0...50 {
            list.append(Model(name: "\(int)", age: "\(Double(int) * 4.25)"))
        }
        callback(list)
    }
}

// MARK: - Run

let viewController = ViewController()
viewController.viewDidLoad()
