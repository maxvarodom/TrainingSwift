import Foundation

typealias Parameter = (name: String, age: String, money: String)

class Person {

    var data: Parameter?

    init() { }

    init(data: Parameter) {
        self.data = data
    }

    func insert(parameter: Parameter) {
        data = parameter
    }

    func get() -> Parameter? {
        return parameter
    }
}

// MARK: - Run

let person = Person()
let parameter = Parameter(name: "Max",
                          age: "22",
                          money: "6000")
person.insert(parameter: parameter)
print(" Parameter -> \(person.get())")
