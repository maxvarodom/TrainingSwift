import UIKit

// MARK: - Base

// BaseViewController
class BaseViewController<ViewModel>: UIViewController where ViewModel: BaseViewModel {

    final var viewModel: ViewModel?

    func viewDidLoad( _ viewModel: ViewModel ){
        super.viewDidLoad()
        self.viewModel = viewModel
    }

    func displayAlertError() {
        print("Error")
    }
}

// BaseViewModel
class BaseViewModel {

}

// BaseProtocol
protocol BaseProtocol: class {
    func displayAlertError()
}

// MARK: - Use Base

class ViewController: BaseViewController<ViewModel>,ViewProtocal {

    override func viewDidLoad() {
        super.viewDidLoad(ViewModel(self))
        viewModel?.getList()
    }

    func displayList(list: [String]) {
        print("แสดงผลรายการ \n\(list)")
    }
}

class ViewModel: BaseViewModel {

    weak var call: ViewProtocal?

    init( _ call: ViewProtocal) {
        self.call = call
    }

    func getList() {
        let list = ["มะม่วง","มะพร้าว","มะละกอ"]
        call?.displayList(list: list)
    }
}

protocol ViewProtocal: BaseProtocol {
    func displayList (list: [String])
}

// MARK: - Run

let viewController = ViewController()
viewController.viewDidLoad()
