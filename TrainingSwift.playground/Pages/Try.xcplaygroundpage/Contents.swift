import Foundation

enum HError: Error {
	case failJobMissing
	case failPrinter001
	case failPrinter002
	case failPrinter003
}

func send(job: Int, toPrinter: String) throws {
	if job == 0 {
		throw HError.failJobMissing
	} else if toPrinter == "Off" {
		throw HError.failPrinter001
	} else if toPrinter == "On" {
		throw HError.failPrinter002
	} else if toPrinter == "Fire" {
		throw HError.failPrinter003
	}
}

func manage(job: Int, toPrinter: String) {
	do {
		try send(job: job, toPrinter: toPrinter)
	} catch HError.failJobMissing {
		print("\(#file): \(#line) \(HError.failJobMissing)")
	} catch let hError as HError {
		print("\(#file): \(#line) \(hError)")
	} catch {
		print("\(#file): \(#line) \(error)")
	}
}

manage(job: 0, toPrinter: "String")
manage(job: 1, toPrinter: "Off")
manage(job: 2, toPrinter: "On")
manage(job: 3, toPrinter: "Fire")
