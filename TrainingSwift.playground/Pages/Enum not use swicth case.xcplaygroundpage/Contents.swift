import Foundation

enum Item {

    case classA
    case classB
    case classC
    case classD

    func showDetail() -> [String]? {
        let dictionary:[Item:[String]] = [
            .classA : ["Gilded Greaves","Flashy Boots","Enchanted Kicks"],
            .classB : ["War Boots","Hermes Select","Uriels Brand"],
            .classC : ["Spear of Longinus","Fafnirs Talon"],
            .classD : ["Claves Sancti","Muramasa"]
        ]
        return dictionary[self]
    }
}

print("Class A - > \(Item.classA.showDetail()!)")
print("Class B - > \(Item.classB.showDetail()!)")
print("Class C - > \(Item.classC.showDetail()!)")
print("Class D - > \(Item.classD.showDetail()!)")
