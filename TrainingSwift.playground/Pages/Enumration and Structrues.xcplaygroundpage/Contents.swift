enum Rank: Int {
	case acr = 1
	case two, three, four, five, six, seven, eight, nine, ten
	case jack, queen, king
	
	func simpleDescription() -> String {
		switch self {
		case .acr:
			return "acr"
		case .jack:
			return "jack"
		case .king:
			return "king"
		default:
			return String.init(rawValue)
		}
	}
}

let ace = Rank.acr
let aceRawValue = ace.rawValue

if let convertedRank = Rank.init(rawValue: 3) {
	let threeDescription = convertedRank.rawValue
}

if let convertedRank = Rank.init(rawValue: 14) {
	fatalError("")
} else {
	print("Yes, it out of range, it should be nil.")
}

enum Suit {
	case spades, hearts, diamondes, clubes
	
	func simpleDescription() -> String {
		switch self {
		case .spades:
			return "Spades"
		case .hearts:
			return "Hearts"
		case .diamondes:
			return "Diamondes"
		case .clubes:
			return "Clubes"
		}
	}
	
	enum Color: String {
		case red = "Red"
		case black = "Black"
	}
	
	func color() -> Suit.Color {
		switch self {
		case .spades:
			return .red
		case .hearts:
			return .black
		case .diamondes:
			return .red
		case .clubes:
			return .black
		}
	}
}

let hearts = Suit.hearts
let heartsDescription = hearts.simpleDescription()

let suit = [
	Suit.clubes,
	Suit.diamondes,
	Suit.hearts,
	Suit.spades
]

suit.forEach { (suti) in
	print(suti.simpleDescription(),
		  suti.color().rawValue)
}

enum ServerResponse {
	case success([AnyHashable: Any])
	case failure((httpStatus: Int, message: String))
}

let success = ServerResponse.success ([
	"items": [
		"name": "รองเท้า",
		"ratings": [
			"bad": 20,
			"soso": 30,
			"good": 50
		]
	]
	])

let fails = ServerResponse.failure((httpStatus: 500, message: "Internal Server Error"))

switch success {
case .success(let message):
	print(message)
case .failure(let error):
	print(error)
default:
	break
}
