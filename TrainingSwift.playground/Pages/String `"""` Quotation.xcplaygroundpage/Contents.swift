import Foundation

example(of: "Quotation") {
	let quotation = """
i send: Max
you send: Hello
"""
	print(quotation)
}
