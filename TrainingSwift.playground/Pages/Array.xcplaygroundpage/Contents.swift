import Foundation

example(of: "Array") {
	var shoppingList = ["catfish","water","tulips","blue paint"]
	shoppingList[1] = "bottle of water"
	print(shoppingList)
}
