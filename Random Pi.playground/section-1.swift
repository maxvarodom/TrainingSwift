import UIKit

for nPoints in [10, 100, 500] {
    var nPointsInside = 0
    
    var view = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
    view.backgroundColor = UIColor.lightGrayColor()

    for _ in 1...nPoints {
        var (x, y) = (drand48() * 2 - 1, drand48() * 2 - 1)
        var point = UIView(frame: CGRect(x: (x + 1) * 50, y: (y + 1) * 50, width: 1, height: 1))

        if x * x + y * y <= 1 {
            ++nPointsInside
            point.backgroundColor = UIColor.redColor()
        } else {
            point.backgroundColor = UIColor.blueColor()
        }
        view.addSubview(point)
    }

    // pi/4 = in / all
    // pi = 4 * in / all

    var pi = 4.0 * Double(nPointsInside) / Double(nPoints)

    view
}

